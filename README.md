# OpenCocon Machine Layer

## Machines on this repository
* ac100 : TOSHIBA AC100 aka. Dynabook AZ
* clover-trail : Clover Trail x86 tablets
* cocon486 : 486, 586, Geode, Transmeta, Cyrix PCs
* coconx64 : First generation amd64 processers 
* primo81 : MSI Primo 81, Allwinner A31s  http://linux-sunxi.org/MSI_Primo81
