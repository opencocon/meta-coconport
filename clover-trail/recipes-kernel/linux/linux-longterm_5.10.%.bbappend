FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

KMACHINE:clover-trail ?= "clover-trail"
COMPATIBLE_MACHINE:clover-trail = "clover-trail"
KCONFIG_MODE ?= "alldefconfig"
KBUILD_DEFCONFIG:clover-trail = "i386_defconfig"

SRC_URI:append:clover-trail += " \
	file://clover-trail-standard.scc \
"

