# based on tune-i586.inc

DEFAULTTUNE ?= "i486"

require conf/machine/include/x86/arch-x86.inc

# Extra tune features
TUNEVALID[i486] = "Enable i486 processor"
TUNE_CCARGS .= "${@bb.utils.contains('TUNE_FEATURES', 'i486', ' -march=i486', '', d)}"

# Extra tune selections
AVAILTUNES += "i486"
TUNE_FEATURES:tune-i486 = "${TUNE_FEATURES:tune-x86} i486"
BASE_LIB:tune-i486 = "lib"
TUNE_PKGARCH:tune-i486 = "i486"
PACKAGE_EXTRA_ARCHS:tune-i486 = "${PACKAGE_EXTRA_ARCHS:tune-x86} i486"

