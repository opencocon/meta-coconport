# look for files in the layer first
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

# workaround : i486 can't generate locale on build
ENABLE_BINARY_LOCALE_GENERATION = "0"

