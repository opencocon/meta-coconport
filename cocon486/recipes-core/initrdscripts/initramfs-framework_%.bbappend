# look for files in the layer first
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += " \
            file://pcmcia \
"

do_install:append() {
	# OpenCocon-local scripts
	install -m 0755 ${WORKDIR}/pcmcia ${D}/init.d/00-pcmcia
}

RDEPENDS:initramfs-module-crusoe:append = " \
                                            pcmciautils \
"
FILES:initramfs-module-crusoe:append = " \
                                        /init.d/00-pcmcia \
"
