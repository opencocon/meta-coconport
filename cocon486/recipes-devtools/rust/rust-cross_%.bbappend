FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

TARGET_ENDIAN[i486] = "little"
TARGET_POINTER_WIDTH[i486] = "32"
TARGET_C_INT_WIDTH[i486] = "32"
MAX_ATOMIC_WIDTH[i486] = "64"
LLVM_TARGET[i486] = "${RUST_TARGET_SYS}"
DATA_LAYOUT[i486] = "e-m:e-p:32:32-f64:32:64-f80:32-n8:16:32-S128"
SRC_URI:append:libc-musl = "\
                            file://i486-unknown-linux-musl.patch \
                            file://patch-compiler_rustc__target_src_spec_mod.patch \
"
