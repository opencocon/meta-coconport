SUMMARY = "Multi-purpose linux bootloader (compatible for older generation machines)"
HOMEPAGE = "http://www.syslinux.org/"
LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://COPYING;md5=0636e73ff0215e8d672dc4c32c317bb3 \
                    file://README;beginline=35;endline=41;md5=f7249a750bc692d1048b2626752aa415"

#DEPENDS = "nasm-native util-linux e2fsprogs"

SRC_URI = "https://mirrors.edge.kernel.org/pub/linux/utils/boot/syslinux/syslinux-${PV}.zip;subdir=syslinux-${PV} \
           "
SRC_URI[sha256sum] = "b763af3c1747be445915f888558a345b8998fcea7a26abe96544c62495fa23b2"

S = "${WORKDIR}/syslinux-${PV}"

# We can build the native parts anywhere, but the target has to be x86
COMPATIBLE_HOST = '(x86_64|i.86).*-(linux|freebsd.*)'

# Don't let the sanity checker trip on the 32 bit real mode BIOS binaries
INSANE_SKIP:${PN} = "arch already-stripped"
INSANE_SKIP:${PN}-menu = "arch"
INSANE_SKIP:${PN}-isolinux = "arch"

do_compile() {
	:
}

do_install() {
	install -d ${D}${bindir}
        install ${S}/linux/syslinux ${D}${bindir}/syslinux4

	install -d ${D}${datadir}/${BPN}/
	install ${S}/core/isolinux.bin ${D}${datadir}/${BPN}/isolinux.bin
	install ${S}/com32/menu/menu.c32 ${D}${datadir}/${BPN}/menu.c32
}


PACKAGES += "${PN}-menu ${PN}-isolinux"

RDEPENDS:${PN} += "libext2fs"

FILES:${PN} = "${bindir}/syslinux4"
FILES:${PN}-menu = "${datadir}/${BPN}/menu.c32"
FILES:${PN}-isolinux = "${datadir}/${BPN}/isolinux.bin"

BBCLASSEXTEND = "native nativesdk"
