FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

KMACHINE:cocon486 ?= "cocon486"
COMPATIBLE_MACHINE:cocon486 = "cocon486"
KCONFIG_MODE ?= "alldefconfig"
KBUILD_DEFCONFIG:cocon486 = "i386_defconfig"

SRC_URI:append:cocon486 = "file://cocon486-standard-5.15.scc"

