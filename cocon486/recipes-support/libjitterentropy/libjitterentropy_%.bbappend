FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

# i386 and ppc needs libssp_noshared (for musl)
CFLAGS:append:libc-musl = " -lssp_nonshared "
LDFLAGS:append:libc-musl = " -lssp_nonshared "
