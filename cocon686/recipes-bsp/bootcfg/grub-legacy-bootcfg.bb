DESCRIPTION = "menu.lst for cocon686"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

COMPATIBLE_MACHINE = "cocon686"

SRC_URI = "file://menu.lst \
          "

S = "${WORKDIR}"

BOOTFILES_DIR_NAME = "/boot/grub"

inherit deploy nopackages

do_deploy() {
    install -d ${DEPLOYDIR}/${BOOTFILES_DIR_NAME}

    cp ${S}/menu.lst ${DEPLOYDIR}/${BOOTFILES_DIR_NAME}/

}
addtask deploy after do_install
