PACKAGE_INSTALL += " \
        pcmciautils \
        kernel-module-ehci-hcd \
        kernel-module-ehci-pci \
        kernel-module-ehci-platform \
        kernel-module-firewire-core \
        kernel-module-firewire-sbp2 \
        kernel-module-i82365 \
        kernel-module-ohci-hcd \
	kernel-module-ohci-platform \
        kernel-module-ohci-pci \
        kernel-module-uhci-hcd \
        kernel-module-usb-common \
        kernel-module-usb-storage \
        kernel-module-usbcore \
        kernel-module-xhci-hcd \
        kernel-module-xhci-pci \
        kernel-module-xhci-plat-hcd \
        kernel-module-yenta-socket \
"

