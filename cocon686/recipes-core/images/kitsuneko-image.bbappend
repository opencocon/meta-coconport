# look for files in the layer first
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

inherit hd_image-pcbios

IMAGE_FSTYPES += "pcbios-hdimg.gz \
                  pcbios-hdimg.gz.md5sum \
                  pcbios-hdimg.vdi.xz \
                  pcbios-hdimg.vdi.xz.md5sum \
                  pcbios-hdimg.qcow2.xz \
                  pcbios-hdimg.qcow2.xz.md5sum \
"
