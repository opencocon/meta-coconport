# look for files in the layer first
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

XSERVER_GL = " \
libegl-mesa \
libgl-mesa \
libglapi \
libgles1-mesa \
libgles2-mesa \
mesa-megadriver \
xserver-xorg-extension-glx \
libgbm \
libdrm-nouveau \
xf86-video-nouveau \
xf86-video-xgixp \
xf86-video-openchrome \
xf86-video-ati \
"

XSERVER = " \
libdrm \
libdrm-intel \
libdrm-kms \
libdrm-radeon \
microcode-xf86-video-rendition \
xf86-input-evdev \
xf86-input-keyboard \
xf86-input-mouse \
xf86-input-synaptics \
xf86-video-apm \
xf86-video-ark \
xf86-video-ast \
xf86-video-chips \
xf86-video-cirrus \
xf86-video-fbdev \
xf86-video-geode \
xf86-video-i128 \
xf86-video-i740 \
xf86-video-intel \
xf86-video-mach64 \
xf86-video-mga \
xf86-video-modesetting \
xf86-video-neomagic \
xf86-video-nv \
xf86-video-r128 \
xf86-video-rendition \
xf86-video-s3virge \
xf86-video-savage \
xf86-video-siliconmotion \
xf86-video-sis \
xf86-video-sis671 \
xf86-video-sisimedia \
xf86-video-sisusb \
xf86-video-tdfx \
xf86-video-trident \
xf86-video-tseng \
xf86-video-vesa \
xf86-video-voodoo \
xf86-video-xgi \
xserver-xorg \
xserver-xorg-module-exa \
xserver-xorg-module-libint10 \
xserver-xorg-module-libwfb \
${XSERVER_GL} \
"
