FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

KMACHINE:cocon686 ?= "cocon686"
COMPATIBLE_MACHINE:cocon686 = "cocon686"
KCONFIG_MODE ?= "alldefconfig"
KBUILD_DEFCONFIG:cocon686 = "i386_defconfig"

SRC_URI:append:cocon686 := "\
                            file://cocon686-standard-6.1.scc \
"

