# look for files in the layer first
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

GRUB_COMPATIBLE_HOST = '(x86_64.*|i.86.*|arm.*|aarch64.*|riscv.*|powerpc.*)-(linux.*|freebsd.*)'
GRUBPLATFORM:powerpc = "ieee1275"
