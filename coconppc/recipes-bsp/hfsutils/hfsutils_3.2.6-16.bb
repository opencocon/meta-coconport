DESCRIPTION = "HFS FS Access utils"
LICENSE = "GPL-2.0-or-later"

inherit autotools-brokensep

# Patches from Gentoo
SRC_URI = "\
	 git://salsa.debian.org/debian/hfsutils.git;protocol=https;branch=master \
         file://hfsutils-3.2.6_p15-Include-string.h-for-strcmp.patch \
         file://hfsutils-3.2.6_p15-drop-manual-autoconf.patch \
"

LIC_FILES_CHKSUM = "file://COPYING;md5=18810669f13b87348459e611d31ab760"

SRCREV = "1bad0ba6fa61e26d5299cc2b3f7a421698bd4574"

EXTRA_OECONF = " --without-tcl --without-tk "

S = "${WORKDIR}/git"

do_install() {
        install -d ${D}${bindir}/
        install -d ${D}${mandir}/man1/
        install -d ${D}${infodir}/

        oe_runmake install prefix=${D}/usr/ BINDEST=${D}${bindir} MANDEST=${D}${mandir} infodir=${D}${infodir}
}

BBCLASSEXTEND = "native"
