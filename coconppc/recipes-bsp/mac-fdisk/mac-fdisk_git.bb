DESCRIPTION = "Mac/PowerMac disk partitioning utility"
LICENSE = "GPL-1.0-or-later & mac-fdisk & BSD-4-Clause-UC"
LIC_FILES_CHKSUM = "file://fdisk.c;beginline=3;endline=8;md5=767873ff31ef260a5a95c298986ecba1 \
                    file://pdisk.c;beginline=10;endline=27;md5=8c38f3b4bb91666c4ac05e2437b0ebe6 \
                    file://fdisklabel.c;beginline=6;endline=35;md5=bbef7c4f3bad35b9df990778a5c93cff"

SRCREV = "bda743065fa2c75a83fec60166bc2e317059ef7a"
# patches from Gentoo
SRC_URI = "git://github.com/glaubitz/mac-fdisk-debian.git;branch=master;protocol=https \
           file://largerthan2gb.patch \
           file://mac-fdisk-0.1-headers.patch \
           file://mac-fdisk-0.1_p16-ppc64.patch \
           file://mac-fdisk-amd64.patch \
           file://big_pt.patch \
           file://mac-fdisk-0.1_p16-ppc-inline.patch \
           file://mac-fdisk-0.1_p18-lseek64.patch \
           file://mac-fdisk-0.1_p18-2tb.patch \
"

SRC_URI:append:libc-musl := "file://mac-fdisk-0.1_p18-musl.patch"

S = "${WORKDIR}/git"
CFLAGS:append:libc-musl := " -Dloff_t=off_t "

do_compile() {
        oe_runmake 
}

do_install() {
        install -d ${D}${base_sbindir}/
        install -m 0755 ${S}/pdisk ${D}${base_sbindir}/mac-fdisk
        install -m 0755 ${S}/fdisk ${D}${base_sbindir}/pmac-fdisk
}

BBCLASSEXTEND = "native"
