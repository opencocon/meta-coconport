FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

KMACHINE:coconppc ?= "coconppc"
COMPATIBLE_MACHINE:coconppc = "coconppc"
KCONFIG_MODE ?= "alldefconfig"
KBUILD_DEFCONFIG:coconppc = "pmac32_defconfig"

SRC_URI:append:coconppc = "file://coconppc-standard-6.6.scc"
