DEFAULTTUNE ?= "genericx64-64"

require conf/machine/include/x86/tune-i686.inc

TUNEVALID[genericx64] = "Generic older x64 cpu tunes"
TUNE_CCARGS .= "${@bb.utils.contains("TUNE_FEATURES", "genericx64", " -march=x86-64 -mtune=generic -mfpmath=sse ", "", d)}"

AVAILTUNES += "genericx64-64"
TUNE_FEATURES:tune-genericx64-64 = "${TUNE_FEATURES:tune-x86-64} genericx64"
BASE_LIB:tune-genericx64-64 = "lib64"
TUNE_PKGARCH:tune-genericx64-64 = "genericx64-64"
PACKAGE_EXTRA_ARCHS:tune-genericx64-64 = "${PACKAGE_EXTRA_ARCHS:tune-i686} genericx64-64"

