IMAGE_FSTYPES += "wic.xz wic.xz.md5sum wic.vdi.xz wic.vdi.xz.md5sum wic.qcow2.xz wic.qcow2.xz.md5sum"
WKS_FILE = "${BPN}.wks.in"
WKS_FILE_DEPENDS = "wic-tools"
RDEPENDS:${PN} = "grub-efi"
