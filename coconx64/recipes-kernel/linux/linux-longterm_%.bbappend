FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

KMACHINE:coconx64 ?= "coconx64"
COMPATIBLE_MACHINE:coconx64 = "coconx64"
KCONFIG_MODE ?= "alldefconfig"
KBUILD_DEFCONFIG:coconx64 = "x86_64_defconfig"

SRC_URI:append:coconx64 = "file://coconx64-standard.scc"

DEPENDS += "elfutils-native"
