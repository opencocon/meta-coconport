# Released under the MIT license (see COPYING.MIT for the terms)

DESCRIPTION = "Packagegroup for Firmwares"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit packagegroup

RDEPENDS:${PN} = " \
  b43-fwcutter \ 
  linux-firmware-rtl8188 \
  linux-firmware-ralink \
  linux-firmware-vt6656 \
  zd1211-firmware \
"
