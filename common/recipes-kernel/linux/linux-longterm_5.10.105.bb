SECTION = "kernel"
DESCRIPTION = "Longterm Linux kernel"
LICENSE = "GPL-2.0-only"

KBRANCH ?= "master"

require recipes-kernel/linux/linux-yocto.inc

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

SRC_URI = "${KERNELORG_MIRROR}/linux/kernel/v5.x/linux-${PV}.tar.xz;name=kernel \
	file://coconport-common.scc \
"
LINUX_VERSION_EXTENSION ?= "-${MACHINE}"

S = "${WORKDIR}/linux-${PV}"

SRC_URI[kernel.sha256sum] = "3ddc0e0ab2b9cadb64df43141e0a1e5432b5963ed50f34d586c065ac8d4fcb85"
