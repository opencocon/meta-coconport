SECTION = "kernel"
DESCRIPTION = "Longterm Linux kernel"
LICENSE = "GPL-2.0-only"

KBRANCH ?= "master"

require recipes-kernel/linux/linux-yocto.inc

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

SRC_URI = "${KERNELORG_MIRROR}/linux/kernel/v6.x/linux-${PV}.tar.xz;name=kernel \
	file://coconport-common.scc \
"
LINUX_VERSION_EXTENSION ?= "-${MACHINE}"

S = "${WORKDIR}/linux-${PV}"

SRC_URI[kernel.sha256sum] = "aaa824eaf07f61911d22b75ff090a403c3dd0bd73e23933e0bba8b5971436ce1"
