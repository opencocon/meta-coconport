FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append:primo81 += "file://primo81-standard.scc \
"

KMACHINE_primo81 ?= "primo81"
COMPATIBLE_MACHINE_primo81 = "primo81"
KCONFIG_MODE ?= "alldefconfig"
KBUILD_DEFCONFIG_primo81 = "sunxi_defconfig"

# Pull in the devicetree files into the rootfs
RDEPENDS:${KERNEL_PACKAGE_NAME}-base += "kernel-devicetree"

KERNEL_EXTRA_ARGS += "LOADADDR=${UBOOT_ENTRYPOINT}"
