FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append_ac100 += "file://ac100-standard.scc \
"

KMACHINE_ac100 ?= "ac100"
COMPATIBLE_MACHINE_ac100 = "ac100"
KCONFIG_MODE ?= "alldefconfig"
KBUILD_DEFCONFIG_ac100 = "tegra_defconfig"

# Pull in the devicetree files into the rootfs
RDEPENDS:${KERNEL_PACKAGE_NAME}-base += "kernel-devicetree"


# Combine kernel and dtb : for Redboot
do_compile:append() {
    # oe_runmake dtbs

    # Combine (Adhoc)
    mv ${S}/arch/arm/boot/zImage ${S}/arch/arm/boot/zImage.org
    cat ${S}/arch/arm/boot/zImage.org arch/arm/boot/dts/tegra20-paz00.dtb > ${S}/arch/arm/boot/zImage
}
