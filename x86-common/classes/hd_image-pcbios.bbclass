inherit image_types

# based sdcard_image-rpi.bbclass from meta-raspberrypi,
#  https://github.com/agherzan/meta-raspberrypi

# Create an image that can be written onto a HDD image using dd.

# This image depends on the rootfs image
IMAGE_TYPEDEP:pcbios-hdimg.gz = "${HDIMG_ROOTFS_TYPE}"

# Set boot loader
IMAGE_BOOTLOADER ?= "grub-legacy"

# Boot partition volume id
BOOTDD_VOLUME_ID ?= "${MACHINE}"

# Boot partition size [in KiB] (will be rounded up to IMAGE_ROOTFS_ALIGNMENT)
BOOT_SPACE ?= "20480"

# Set alignment : from fdisk [KiB]
IMAGE_ALIGNMENT ?= "8033"

# Set alignment in head of image [in KiB]
# for DOS : 63 sector,  63 sector * 512 B = 32256 B = 31 KiB
IMAGE_ROOTFS_ALIGNMENT = "31"

# Use an uncompressed ext3 by default as rootfs
HDIMG_ROOTFS_TYPE ?= "ext3"
HDIMG_ROOTFS = "${IMGDEPLOYDIR}/${IMAGE_LINK_NAME}.${HDIMG_ROOTFS_TYPE}"

# For the names of kernel artifacts
inherit kernel-artifact-names

do_image_pcbios_hdimg[depends] = " \
    parted-native:do_populate_sysroot \
    mtools-native:do_populate_sysroot \
    dosfstools-native:do_populate_sysroot \
    grub-legacy-native:do_populate_sysroot \
    qemu-system-native:do_populate_sysroot \
    util-linux-native:do_populate_sysroot \
    virtual/kernel:do_deploy \
    ${IMAGE_BOOTLOADER}:do_deploy \
    ${IMAGE_BOOTLOADER}-bootcfg:do_deploy \
"

do_image_pcbios_hdimg[recrdeps] = "do_build"

# Hard disk image name
HDIMG = "${IMGDEPLOYDIR}/${IMAGE_NAME}${IMAGE_NAME_SUFFIX}.pcbios-hdimg"

# Additional files and/or directories to be copied into the vfat partition from the IMAGE_ROOTFS.
FATPAYLOAD ?= ""

# HD vfat partition image name
HDIMG_VFAT_DEPLOY ?= "${RPI_USE_U_BOOT}"
HDIMG_VFAT = "${IMAGE_NAME}.vfat"
HDIMG_LINK_VFAT = "${IMGDEPLOYDIR}/${IMAGE_LINK_NAME}.vfat"


IMAGE_CMD:pcbios-hdimg () {

    # Align partitions
    BOOT_SPACE_ALIGNED=$(expr ${BOOT_SPACE} + \( ${BOOT_SPACE} % ${IMAGE_ALIGNMENT} \) )
    ROOT_SPACE_ALIGNED=$(expr ${ROOTFS_SIZE} + \( ${ROOTFS_SIZE} % ${IMAGE_ALIGNMENT} \) ) 
    HDIMG_SIZE=$(expr ${IMAGE_ROOTFS_ALIGNMENT} + ${BOOT_SPACE_ALIGNED} + ${ROOT_SPACE_ALIGNED} )

    echo "Creating filesystem with Boot partition ${BOOT_SPACE_ALIGNED} KiB and RootFS $ROOTFS_SIZE KiB"

    # Initialize hard disk image file
    dd if=/dev/zero of=${HDIMG} bs=1024 count=0 seek=${HDIMG_SIZE}

    # Create partition layout using fdisk
    # for make correct CHS.
    fdisk ${HDIMG} -c=dos << EOF
n
p
1

+${BOOT_SPACE}K
t
6
a
n
p
2

+${ROOTFS_SIZE}K
t
2
83
p
w
EOF

    parted ${HDIMG} print

    # Get start point of each partition [Byte]
    BOOT_START=$(LC_ALL=C parted -s ${HDIMG} unit b print | awk '/ 1 / { print substr($2, 1, length($4 -1)) * 1 }')
    ROOT_START=$(LC_ALL=C parted -s ${HDIMG} unit b print | awk '/ 2 / { print substr($2, 1, length($4 -1)) * 1 }')

    # Create a vfat image with boot files
    BOOT_BLOCKS=$(LC_ALL=C parted -s ${HDIMG} unit b print | awk '/ 1 / { print int( substr($4, 1, length($4 -1)) / 512 /2 ) }')
    echo "Boot Blocks : ${BOOT_BLOCKS}"

    ## Create FAT temporary image to boot.img
    rm -f ${WORKDIR}/boot.img
    mkfs.vfat -F16 -n "$( echo ${BOOTDD_VOLUME_ID} | tr '[:upper:]' '[:upper:]' )" -S 512 -C ${WORKDIR}/boot.img $BOOT_BLOCKS

    ## Copy bootloader files
    ## TODO : currently is grub-legacy only. add grub2 later.
    BOOTLOADER_COPY="/boot/grub/*"
    mmd -i ${WORKDIR}/boot.img ::/grub
    mcopy -v -i ${WORKDIR}/boot.img -s ${DEPLOY_DIR_IMAGE}/${BOOTLOADER_COPY} ::/grub/ || bbfatal "mcopy cannot copy ${DEPLOY_DIR_IMAGE}/${BOOTLOADER_COPY}"

    # Copy board device trees to root folder
    KERNEL_COPY="/bzImage"
    mcopy -v -i ${WORKDIR}/boot.img -s ${DEPLOY_DIR_IMAGE}/${KERNEL_COPY} :: || bbfatal "mcopy cannot copy ${DEPLOY_DIR_IMAGE}/${KERNEL_COPY} into boot.img"

    # Add files (eg. hypervisor binaries) from the deploy dir
    if [ -n "${DEPLOYPAYLOAD}" ] ; then
        echo "Copying deploy file payload into VFAT"
        for entry in ${DEPLOYPAYLOAD} ; do
            # Split entry at optional ':' to enable file renaming for the destination
            if [ $(echo "$entry" | grep -c :) = "0" ] ; then
                DEPLOY_FILE="$entry"
                DEST_FILENAME="$entry"
            else
                DEPLOY_FILE="$(echo "$entry" | cut -f1 -d:)"
                DEST_FILENAME="$(echo "$entry" | cut -f2- -d:)"
            fi
            mcopy -v -i ${WORKDIR}/boot.img -s ${DEPLOY_DIR_IMAGE}/${DEPLOY_FILE} ::${DEST_FILENAME} || bbfatal "mcopy cannot copy ${DEPLOY_DIR_IMAGE}/${DEPLOY_FILE} into boot.img"
        done
    fi

    if [ -n "${FATPAYLOAD}" ] ; then
        echo "Copying payload into VFAT"
        for entry in ${FATPAYLOAD} ; do
            # use bbwarn instead of bbfatal to stop aborting on vfat issues like not supporting .~lock files
            mcopy -v -i ${WORKDIR}/boot.img -s ${IMAGE_ROOTFS}$entry :: || bbwarn "mcopy cannot copy ${IMAGE_ROOTFS}$entry into boot.img"
        done
    fi

    # Add stamp file
    echo "${IMAGE_NAME}" > ${WORKDIR}/IMGNAME.TXT
    mcopy -v -i ${WORKDIR}/boot.img ${WORKDIR}/IMGNAME.TXT :: || bbfatal "mcopy cannot copy ${WORKDIR}/IMGNAME.TXT into boot.img"

    # Deploy vfat partition
    if [ "${HDIMG_VFAT_DEPLOY}" = "1" ]; then
        cp ${WORKDIR}/boot.img ${IMGDEPLOYDIR}/${HDIMG_VFAT}
        ln -sf ${HDIMG_VFAT} ${HDIMG_LINK_VFAT}
    fi

    # Burn Boot Partition
    dd if=${WORKDIR}/boot.img of=${HDIMG} conv=notrunc seek=1 bs=${BOOT_START}

    # Burn Root Partition
    dd if=${HDIMG_ROOTFS} of=${HDIMG} conv=notrunc seek=1 bs=${ROOT_START}


    # Install GRUB legacy
    # grub 0.97 is always send segmentation fault on quit, then ignore this using set +e .
    # Since grub 0.97 is so old, show some warning, but No problem where install the bootloader to disk image.
    set +e

    grub --batch --no-floppy <<EOF
device (hd0) ${HDIMG}
root (hd0,0)
setup (hd0)
quit
EOF

    set -e

}
