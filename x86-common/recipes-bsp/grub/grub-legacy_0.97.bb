SUMMARY = "GRUB is the GRand Unified Bootloader"
DESCRIPTION = "GRUB is a GPLed bootloader intended to unify bootloading across x86 \
operating systems. In addition to loading the Linux kernel, it implements the Multiboot \
standard, which allows for flexible loading of multiple boot images."
HOMEPAGE = "http://www.gnu.org/software/grub/"
SECTION = "bootloaders"

LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://COPYING;md5=c93c0550bd3173f4504b2cbd8991e50b \
                    file://grub/main.c;beginline=3;endline=9;md5=22a5f28d2130fff9f2a17ed54be90ed6"

RDEPENDS:${PN} = "diffutils ncurses-libncurses ncurses-libtinfo"
DEPEND += "ncurses gnu-config-native"

SRCREV = "78e67ac21bb2f2ac223bc53c815172e75a64ba20"
PV = "0.9.7-80"
PR = "gitr${SRCREV}"
S = "${WORKDIR}/git"


SRC_URI = "git://salsa.debian.org/grub-team/grub-legacy.git;branch=master;protocol=https \
           file://grub-binutils_2.34-PHDR-segment.patch \
           file://objcopy-add-strip-section.patch \
"
 
SRC_URI:append:libc-musl = "file://grub-legacy-musl-v2.patch"

inherit autotools deploy 

BBCLASSEXTEND = "native nativesdk"

# x86_64-linux
COMPATIBLE_HOST = ".*86.*-linux"

# Disable optimize flag
CFLAGS:append = " -O0 -fno-reorder-functions -fno-stack-protector -fno-strict-aliasing -fno-strength-reduce -fno-unroll-loops"
LDFLAGS:append = " -O0 "

# do_deploy : for make disk image.
do_deploy() {
    if [ "${PN}" != "grub-legacy-native" ]; then
        install -d ${DEPLOYDIR}/boot/grub
        cp ${D}/${libdir}/grub/i386${TARGET_VENDOR}/* ${DEPLOYDIR}/boot/grub/
    fi
}
addtask deploy after do_install


# TODO : remove dupliate
FILES:${PN} += "/boot/* \
                ${libdir}/grub/i386-pc/*"

do_deploy[dirs] += "${DEPLOYDIR}/boot/grub"
