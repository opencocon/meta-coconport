DESCRIPTION = "Opengalax Touchpad daemon"
HOMEPAGE = "https://github.com/poliva/opengalax"
LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://LICENSE;md5=cbbd794e2a0a289b9dfcc9f513d1996e"

SRC_URI = "git://github.com/oskarirauta/opengalax2.git;protocol=https;branch=master \
"
SRCREV = "fe4cb84f775e26a447602102914da9713dd8bc9d"
S = "${WORKDIR}/git"

DEPENDS = "tslib"

do_configure() {
        # Some modify to Makefile
        sed -i -e "/^CC/d" ${S}/Makefile
        sed -i -e "1i CC=${TARGET_PREFIX}gcc ${TOOLCHAIN_OPTIONS}" ${S}/Makefile

        sed -i -e "/^CFLAGS/d" ${S}/Makefile
        sed -i -e "1i CFLAGS=${CFLAGS}" ${S}/Makefile

        sed -i -e "/^LDFLAGS/d" ${S}/Makefile
        sed -i -e "1i LDFLAGS=${LDFLAGS} -z muldefs" ${S}/Makefile

        sed -i -e "/^DESTDIR/d" ${S}/Makefile
        sed -i -e "1i DESTDIR=${D}" ${S}/Makefile
}

do_compile() {
	cd ${S}
        oe_runmake 
}

do_install() {
	cd ${S}

	oe_runmake install

	# TODO : copy init.d script
}

