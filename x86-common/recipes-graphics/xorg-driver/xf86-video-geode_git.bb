require recipes-graphics/xorg-driver/xorg-driver-video.inc
LIC_FILES_CHKSUM = "file://COPYING;md5=e7f3e39474aeea5af381a8e103dafc36"

SUMMARY = "X.org server -- Geode GX2/LX display driver"

COMPATIBLE_HOST = "i.86.*-linux"

RDEPENDS:${PN} += "xserver-xorg-module-exa"

SRC_URI = "git://gitlab.freedesktop.org/xorg/driver/xf86-video-geode.git;protocol=https;branch=master"

S = "${WORKDIR}/git"
SRCREV = "0bb81df318e2dbd093040ba35a51b726b8954cb5"
