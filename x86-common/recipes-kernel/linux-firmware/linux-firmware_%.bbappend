# Marvell Libertas
LICENSE:${PN}-sd8686-v8 = "Firmware-Marvell"
LICENSE:${PN}-sd8385 = "Firmware-Marvell"

FILES:${PN}-sd8686-v8 = " \
  ${nonarch_base_libdir}/firmware/libertas/sd8686_v8* \
"

FILES:${PN}-sd8385 = " \
  ${nonarch_base_libdir}/firmware/libertas/sd8385* \
"

RDEPENDS:${PN}-sd8686-v8 += "${PN}-marvell-license"
RDEPENDS:${PN}-sd8385 += "${PN}-marvell-license"

PACKAGES =+ " \
             ${PN}-sd8686-v8 ${PN}-sd8385 \
"
