#!/bin/bash


usage() {
  echo "Usage: $(basename "$0") [options]"
  echo " -c (dir) ---- coconcd directory"
  echo " -d (string) ---- Distribution name [opencocon, kitsuneko]"
  echo " -i (dir) ---- Deploy image directory"
  echo " -l (string) ---- Target libc name [glibc, musl]"
  echo " -m (string) ---- Target machine name [cocon486, cocon686]"
  echo " -n (string) ---- Image name"
  echo " -o (dir) ---- Output directory"
  echo " -S (dir) ---- syslinux 4.07 directory"
  echo " -w (dir) ---- Work directory"
  echo ""
  echo "This script is for syslinux4 OpenCocon image." 
  exit 1
}

# Set default values
DISTRO_NAME="opencocon"
COCON_PLATHOME_LIBC="musl"
COCON_PLATHOME="cocon486"
IMAGE_DATE="snapshot-$(date +%Y%m%d)"
COCONCD_DIR="$(dirname "$0")/coconcd"
RAMDISK_SIZE="9000"

# read options
if [ -z "$1" ];
then
  usage
fi 

while getopts ':c:d:i:l:m:n:o:S:w:' OPTION; do
  case "$OPTION" in

  c)
    COCONCD_DIR="$OPTARG" ;;

  d)
    DISTRO_NAME="$OPTARG" ;;

  i)
    DEPLOYIMG_DIR="$OPTARG"

    # Check deploy directory exists
    if [ ! -d "$DEPLOYIMG_DIR" ];
    then
      echo "Error: Deploy image directory is not exists."
      exit 2
    fi
    ;;
 
  l)
    COCON_PLATHOME_LIBC="$OPTARG" ;;

  m)
    COCON_PLATHOME="$OPTARG" ;;

  n)
    IMAGE_DATE="$OPTARG" ;;

  o)
    OUTPUT_DIR="$OPTARG"

    # Check output directory exists
    if [ ! -d "$OUTPUT_DIR" ];
    then
      # make outut directory
      if ! mkdir -p "$OUTPUT_DIR" ;
      then
        echo "Error: Failed to make Output directory."
        exit 2
      fi 
    fi
    ;;

  S)
    # Define syslinux Parameters
    SYSLINUX_PATH="$OPTARG"
    # SYSLINUX_BIN_PATH="$SYSLINUX_PATH/linux/"
    SYSLINUX_CORE_PATH="$SYSLINUX_PATH/core/"
    SYSLINUX_MODULES_PATH="$SYSLINUX_PATH/com32/"

    # Check isolinux file exists
    if [ ! -r "$SYSLINUX_CORE_PATH/isolinux.bin" ] || [ ! -r "$SYSLINUX_MODULES_PATH/menu/menu.c32" ];
    then
      echo "Error: nesesary file on syslinux is not found."
      exit 2
    fi
    ;;

  w)
    # Define Work Directory
    WORKDIR="$OPTARG" ;;

  ?)
    usage ;;

  esac
done


# Check parameters
if [ -z "$DEPLOYIMG_DIR" ] || [ -z "$WORKDIR" ] || [ -z "$SYSLINUX_PATH" ] || [ -z "$OUTPUT_DIR" ];
then
  echo "Error: Nesesary parameter is missing."
  exit 2
fi

# check plathome, libc and decide boot ramdisk size
if [ "$COCON_PLATHOME" = "cocon486" ];
then
  if [ "$COCON_PLATHOME_LIBC" = "musl" ];
  then
    RAMDISK_SIZE="6000"
  elif [ "$COCON_PLATHOME_LIBC" = "glibc" ];
  then
    RAMDISK_SIZE="8000"
  else
    echo "Error: Unsupported libc."
    exit 2
  fi
elif [ "$COCON_PLATHOME" = "cocon686" ];
then
   if [ "$COCON_PLATHOME_LIBC" = "musl" ];
  then
    RAMDISK_SIZE="9000"
  elif [ "$COCON_PLATHOME_LIBC" = "glibc" ];
  then
    RAMDISK_SIZE="10000"
  else
    echo "Error: Unsupported libc."
    exit 2
  fi
else
  echo "Error: Unsupported plathome."
  exit 2
fi

# Check nesesary program
if [ ! "$( command -v genisoimage )" ];
then
  echo "Error: genisoimage is not found."
  exit 2
fi

# Check nesesary deploy image files
if [ ! -r "$DEPLOYIMG_DIR/bzImage-$COCON_PLATHOME.bin" ] || [ ! -r "$DEPLOYIMG_DIR/$DISTRO_NAME-$COCON_PLATHOME.squashfs" ] || [ ! -r "$DEPLOYIMG_DIR/crusoe-$COCON_PLATHOME.squashfs" ];
then
  echo "Error: Nesesary file on deploy image is not found."
  exit 2
fi

# Create work directory
if ! mkdir -p "$WORKDIR"/isolinux/ ;
then
  echo "Error: Failed to make Work directory."
  exit 2
fi 
 
# Copy isolinux
mkdir -p "$WORKDIR"/isolinux/
cp -L "$SYSLINUX_CORE_PATH"/isolinux.bin  "$WORKDIR"/isolinux/isolinux.bin
cp -L "$SYSLINUX_MODULES_PATH"/menu/menu.c32  "$WORKDIR"/isolinux/menu.c32

# Copy image to work directory
cp -L "$DEPLOYIMG_DIR"/bzImage-"$COCON_PLATHOME".bin  "$WORKDIR"/bzImage
cp -L "$DEPLOYIMG_DIR"/"$DISTRO_NAME"-"$COCON_PLATHOME".squashfs  "$WORKDIR/"crusoe.sqs
cp -L "$DEPLOYIMG_DIR"/crusoe-"$COCON_PLATHOME".squashfs  "$WORKDIR"/early.sqs

# Copy some file from define directory
cp -RL "$COCONCD_DIR"/common/*  "$WORKDIR"/
cp -RL "$COCONCD_DIR"/"$COCON_PLATHOME"/* "$WORKDIR"/

# Include RAMDISK_SIZE to $COCONCD_DIR/isolinux/isolinux.cfg
sed -e "s/___RAMDISK_SIZE___/${RAMDISK_SIZE}/g" "$WORKDIR"/isolinux/isolinux.cfg.in > "$WORKDIR"/isolinux/isolinux.cfg
rm "$WORKDIR"/isolinux/isolinux.cfg.in

# Create CD image
iso_filename="$OUTPUT_DIR/$DISTRO_NAME-${COCON_PLATHOME}_${COCON_PLATHOME_LIBC}-$IMAGE_DATE.iso"
genisoimage -v -r -J -o "$iso_filename" -b isolinux/isolinux.bin -no-emul-boot -boot-load-size 4 -boot-info-table -A "$( echo "$COCON_PLATHOME" | tr '[:lower:]' '[:upper:]' )" "$WORKDIR"/

if [ ! -r "$iso_filename" ];
then 
  echo "Error: genisoimage failed."
  exit 2
fi
 
"$SYSLINUX_PATH"/utils/isohybrid.pl -ctrlhd0 "$iso_filename"

#cd "$CURPWD" || exit 2
#md5sum "$iso_filename" > "${iso_filename}.md5"

echo "make $iso_filename completely."
exit 0
